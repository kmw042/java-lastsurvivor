package com.kmw.lastsurvivor;

/**
 * Exception handling class for LastSurvivor
 */
public class LastSurvivorException extends Exception {

    public LastSurvivorException(String message) {
        super(message);
    }

    public LastSurvivorException(String message, Throwable throwable) {
        super(message, throwable);
    }

}


