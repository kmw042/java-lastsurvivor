package com.kmw.lastsurvivor;

/**
 *  Simple class to determine the answer to this question:
 *  You are in a room with a circle of 100 chairs. The chairs are numbered sequentially from 1 to 100.
 *
 *   At some point in time, the person in chair #1 will be asked to leave. The person in chair #2 will be skipped,
 *   and the person in chair #3 will be asked to leave. This pattern of skipping one person and asking the next
 *   to leave will keep going around the circle until there is one person left� the survivor.
 *
 */


import java.io.IOException;

import java.util.BitSet;
import java.util.Scanner;


public class LastSurvivor {

    // declarations
    //these must be public so they pass by reference in the recursive method
    public BitSet chairs;
    public int currChairNumber;
    public boolean emptyIt;

    /**
     *Default constructor
     */
    public LastSurvivor() {
        super();

        //initialization
        chairs = new BitSet(100);
        chairs.set(1, 101);
        currChairNumber = 1;
        emptyIt = true;
    }

    /**
     * Constructor
     * @param numberOfChairs
     */
    public LastSurvivor(int numberOfChairs) {
        super();

        //initialization
        chairs = new BitSet(numberOfChairs);
        chairs.set(1, numberOfChairs + 1);
    }

    /**
     * Removes all but one chair occupant starting at the first chair and skipping an occupied chair
     * each time
     * @return int - the last occupied chair
     */
    public int removeSittersBitSet() {
        // declarations
        int currentChairNum;
        int lastChairNumber;

        // initializations
        // chair #1 is the first to be emptied
        currentChairNum = 1;
        lastChairNumber = -1;

        while (chairs.cardinality() > 1) {
            // remove the sitter in chair chairNum
            chairs.clear(currentChairNum);

            //make sure there's still more than one survivor
            if (chairs.cardinality() > 1) {
                //get the sitter to skip
                currentChairNum = getNextOccupiedChair(chairs, currentChairNum);
                // get the next sitter to remove
                currentChairNum = getNextOccupiedChair(chairs, currentChairNum);
            } else {
                // found the last occupied chair
                lastChairNumber = getNextOccupiedChair(chairs, currentChairNum);
            }
        }
        return lastChairNumber;
    }

    /**
     * Using recursion, removes all but one chair occupant starting at the first chair and
     * skipping an occupied chair each time.
     * @return int - the last occupied chair
     */
    public synchronized int removeSittersRecursiveBitSet() {
        //initialize
        int i = getNextOccupiedChair(this.chairs, this.currChairNumber);

        // if we are skipping this sitter, remove the next one
        if (this.emptyIt == false) {
            this.emptyIt = true;
        } else {
            // remove this sitter
            this.chairs.clear(i);
            // don't remove the next one
            this.emptyIt = false;
        }

        //update the current chair number
        this.currChairNumber = i;

        // if there is still more than one sitter
        if (this.chairs.cardinality() > 1) {
            this.removeSittersRecursiveBitSet();
        }

        return (this.chairs.nextSetBit(0) - 1);
    }


    /**
     * Find the next occupied chair
     * @param chairSet - BitSet of full/empty chairs
     * @param currChairNum - current position in the set of chairs
     * @return - next occupied chair
     */
    public int getNextOccupiedChair(BitSet chairSet, int currChairNum) {
        // nextSetBit returns the current chair if it's still occupied, so add 1 to the currChairNum
        currChairNum = chairSet.nextSetBit(currChairNum + 1);

        // if we passed the last value, start over at the beginning
        if (currChairNum == -1) {
            currChairNum = chairSet.nextSetBit(0);
        }
        return currChairNum;
    }

    /**
     * main() method - Executes both versions of removeSitters and
     * displays nano time used for each method.
     *
     * @param args -  none
     */
    public static void main(String[] args) {

        // declarations
        // give a relative comparision of execution time
        long nanoStart;
        long timeSpentInNanoTime;

        // for user input
        Scanner inputReader;
        int totalChairs;
        int minChairs;
        int maxChairs;
        String continueRunning;

        LastSurvivor lastChair;
        int survivor;

        // initialization
        minChairs = 1;
        // standard memory configuration results in stack overflow somewhere over 1000
        // chairs, so 1000 is the limit
        maxChairs = 1000;

        try {
            inputReader = new Scanner(System.in);
            System.out.print("Enter number of chairs (range 1-1000) in the circle: ");
            totalChairs = inputReader.nextInt();

            if ((totalChairs < minChairs) || (totalChairs > maxChairs)) {
                throw new LastSurvivorException("Invalid number of chairs.\nGoodbye.");
            }

            //re-start timer
            nanoStart = System.nanoTime();

            if (totalChairs == 1) {
                survivor = 1;
            } else {
                //initialize for non-recursive method
                lastChair = new LastSurvivor(totalChairs);
                survivor = -1;

                //do the work
                survivor = lastChair.removeSittersBitSet();
            }
            System.out.println("The last survivor is in chair number " + survivor + ".");

            //stop timer & print result
            timeSpentInNanoTime = System.nanoTime() - nanoStart;
            System.out.println("Relative elapsed time using BitSet: " + timeSpentInNanoTime + "ns");

            //re-start timer
            nanoStart = System.nanoTime();

            if (totalChairs == 1) {
                survivor = 1;
            } else {
                //re-initialize for recursive method
                lastChair = new LastSurvivor(totalChairs);
                survivor = -1;

                //do the work
                survivor = lastChair.removeSittersRecursiveBitSet();
            }
            System.out.println("The last survivor is in chair number " + survivor + ".");

            //stop timer & print result
            timeSpentInNanoTime = System.nanoTime() - nanoStart;
            System.out.println("Relative elapsed time using recursion: " + timeSpentInNanoTime + "ns");

        } catch (LastSurvivorException lce) {
            System.out.println(lce.getMessage());
            System.exit(1);
        }

    }

}
